return function()
  local image = love.graphics.newImage('art/star.png')
  local s = love.graphics.newParticleSystem(image, 1000)
  s:setParticleLifetime(3)
  s:setEmissionRate(30)
  s:setSpread(math.pi*2)
  s:setSpeed(200, 300)
  s:setRadialAcceleration(500)
  s:setSizes(0.15)
  s:setColors(255, 255, 255, 0,
              255, 255, 255, 128,
              255, 255, 255, 255)
  return s
end
