debug_draw = require 'debug_draw'
starfield = require 'starfield'
tentacle = require 'tentacle'
scissors = require 'scissors'
stapler = require 'stapler'
cup = require 'cup'

toy_box = {
  cup,
  stapler,
  scissors,
}

fixtureCategories = {
  tentacle = 1,
  wall = 2,
}

debug = false
title = true

function love.load()
  world = love.physics.newWorld(0, 0, false)
  world:setCallbacks(play_squelch, nil, nil, nil)

  build_wall(world, 0, -10, 1280, 10)
  build_wall(world, 0, 720, 1280, 10)
  build_wall(world, -10, 0, 10, 720)
  build_wall(world, 1280, 0, 10, 720)

  bg = starfield()
  for i = 1,120 do
    bg:update(0.1)
  end

  bgm = love.audio.newSource('music/bgm.ogg', 'stream')
  bgm:setLooping(true)
  bgm:setVolume(0.6)
  
  tentacle.load()
  for _, module in ipairs(toy_box) do
    if module.load then
      module.load()
    end
  end

  left_tentacle = tentacle.new(world, -100, 900, true)
  right_tentacle = tentacle.new(world, 1380, 900, false)

  squelches = {}
  for i = 1,7 do
    local s = love.audio.newSource('music/squelch0'..i..'.ogg', 'static')
    s:setVolume(0.4)
    table.insert(squelches, s)
  end

  toy_i = 0
  toy = nil

  fade = 0
  fade_dir = 0

  title_image = love.graphics.newImage('art/title.png')
  controls = love.graphics.newImage('art/controls.png')
  complete_msg = love.graphics.newImage('art/complete.png')

  bgm:play()
end

function love.update(dt)
  dt = math.min(dt, 0.1)

  bg:update(dt)
  world:update(dt)
  left_tentacle:update(dt)
  right_tentacle:update(dt)

  fade = fade + fade_dir*dt*3
  if fade_dir > 0 then
    if fade >= 1 then
      fade = 1
      fade_dir = 0
      onFadeOut()
    end
  elseif fade_dir < 0 then
    if fade <= 0 then
      fade = 0
      fade_dir = 0
      onFadeIn()
    end
  end

  if toy and toy.update then
    toy:update(dt)
  end
end

function onFadeOut()
  title = false
  if toy then
    toy:destroy()
  end
  toy = next_toy(640, 200)
  fade_dir = -1
end

function onFadeIn()
end

function love.draw()
  love.graphics.push()
  love.graphics.translate(love.graphics.getWidth()/2, love.graphics.getHeight()/2)
  love.graphics.setColor(255, 255, 255)
  love.graphics.draw(bg)
  love.graphics.pop()

  if title then
    love.graphics.draw(title_image)
  else
    if toy and toy.draw then
      toy:draw()
    end

    left_tentacle:draw()
    right_tentacle:draw()

    love.graphics.draw(controls, 0, 600)
    if toy and toy.complete then
      love.graphics.draw(complete_msg, 400, 0)
    end

    if debug then
      love.graphics.setColor(0, 255, 0)
      debug_draw(world)
    end
  end

  if fade > 0 then
    love.graphics.setColor(0, 0, 0, fade*255)
    love.graphics.rectangle('fill', 0, 0, 1280, 720)
  end
end

function love.keypressed(key)
  if title then
    fade_dir = 1
    return
  end

  if key == 'escape' and debug then
    love.event.push('quit')
  elseif key == '`' then
    debug = not debug
  elseif key == 'n' and fade == 0 and (debug or toy.complete) then
    fade_dir = 1
  end
end

function play_squelch(f1, f2, c)
  if f1:getCategory() == fixtureCategories.tentacle
    or f1:getCategory() == fixtureCategories.tentacle then
    if math.random(3) ~= 1 then
      local i = math.random(#squelches)
      squelches[i]:rewind()
      squelches[i]:play()
    end
  end
end

function build_wall(world, x, y, w, h)
  local body = love.physics.newBody(world, x, y, 'static')
  local s1 = love.physics.newRectangleShape(w/2, h/2, w, h)
  local f1 = love.physics.newFixture(body, s1)
  f1:setCategory(fixtureCategories.wall)

  local corner = 50
  if w < h then
    x = x - w/2
  else
    y = y - h/2
  end
  local s2 = love.physics.newPolygonShape(
                x, y-corner,
                x+corner, y,
                x, y+corner,
                x-corner, y
             )
  if w < h then
    y = y + h
  else
    x = x + w
  end
  local s3 = love.physics.newPolygonShape(
                x, y-corner,
                x+corner, y,
                x, y+corner,
                x-corner, y
             )
  local f2 = love.physics.newFixture(body, s2)
  local f3 = love.physics.newFixture(body, s3)
  f2:setCategory(fixtureCategories.wall)
  f3:setCategory(fixtureCategories.wall)
end

function next_toy(x, y)
  toy_i = toy_i % #toy_box + 1
  return toy_box[toy_i].new(world, x, y)
end
