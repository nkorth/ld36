local tentacle = {}
tentacle.__index = tentacle

function tentacle.load()
  tentacle.images = {}
  for i = 1,4 do
    local filename = "art/tentacle1.000"..i..".png"
    local image = love.graphics.newImage(filename)
    table.insert(tentacle.images, image)
  end
end

function tentacle.new(world, x, y, left)
  local t = {}
  setmetatable(t, tentacle)
  t.world = world
  t.init_x = x
  t.init_y = y

  local tentacle_shape1 = {
    -20, -600,
    -40, -850,
    0, -900,
    90, -650,
    100, -500,
    70, -300,
  }
  local tentacle_shape2 = {
    -80, 0,
    -120, -200,
    -20, -600,
    70, -300,
    130, 0,
  }
  if not left then
    for i, n in ipairs(tentacle_shape1) do
      if i % 2 == 1 then
        tentacle_shape1[i] = -n
      end
    end
    for i, n in ipairs(tentacle_shape2) do
      if i % 2 == 1 then
        tentacle_shape2[i] = -n
      end
    end
  end

  t.body = love.physics.newBody(world, x, y, 'dynamic')
  t.body:setAngularDamping(20)
  t.shape1 = love.physics.newPolygonShape(tentacle_shape1)
  t.shape2 = love.physics.newPolygonShape(tentacle_shape2)
  t.fixture1 = love.physics.newFixture(t.body, t.shape1)
  t.fixture1:setCategory(fixtureCategories.tentacle)
  t.fixture1:setMask(fixtureCategories.wall)
  t.fixture2 = love.physics.newFixture(t.body, t.shape2)
  t.fixture2:setCategory(fixtureCategories.tentacle)
  t.fixture2:setMask(fixtureCategories.wall)

  t.pivot_body = love.physics.newBody(world, x, y, 'kinematic')
  t.pivot_shape = love.physics.newCircleShape(10)
  love.physics.newFixture(t.pivot_body, t.pivot_shape)

  t.pivot_joint = love.physics.newRevoluteJoint(t.body, t.pivot_body, x, y, false)
  t.pivot_joint:setLimitsEnabled(true)

  t.left = left
  if left then
    t.body:setAngle(0.8)
    t.anim_t = 0
  else
    t.body:setAngle(-0.8)
    t.anim_t = 1.5
  end
  return t
end

function tentacle:update(dt)
  local anim_speed = 12
  self.anim_t = (self.anim_t + dt*anim_speed) % 4

  local forwardKey, leftKey, backKey, rightKey = 'w', 'a', 's', 'd'
  if not self.left then
    forwardKey, leftKey, backKey, rightKey = 'up', 'left', 'down', 'right'
  end

  local rotate_speed = 1.5
  if love.keyboard.isDown(rightKey) then
    self.body:setAngularVelocity(rotate_speed)
  elseif love.keyboard.isDown(leftKey) then
    self.body:setAngularVelocity(-rotate_speed)
  else
    --self.body:setAngularVelocity(0)
  end

  local x, y = self.body:getPosition()
  local move_speed = 500
  if love.keyboard.isDown(forwardKey) then
    y = y - move_speed*dt
  elseif love.keyboard.isDown(backKey) then
    y = y + move_speed*dt
  end
  y = math.max(self.init_y - 120, math.min(y, self.init_y + 300))

  local dy = y - self.init_y
  if self.left then
    x = self.init_x - dy
  else
    x = self.init_x + dy
  end

  self.pivot_body:setPosition(x, y)
  self.body:setPosition(x, y)

  local limit_restrict = dy / 200 * 0.25
  if self.left then
    self.pivot_joint:setLimits(-1.3 + limit_restrict, -0.15 - limit_restrict)
  else
    self.pivot_joint:setLimits(0.15 + limit_restrict, 1.3 - limit_restrict)
  end
end

function tentacle:draw()
  love.graphics.push()
  love.graphics.translate(self.body:getPosition())
  if self.left then
    love.graphics.rotate(self.body:getAngle())
  else
    love.graphics.scale(-1, 1)
    love.graphics.rotate(-self.body:getAngle())
  end
  local i = math.floor(self.anim_t) + 1
  love.graphics.setColor(255, 255, 255)
  love.graphics.draw(self.images[i], -550, -1100, 0, 0.8, 0.8)
  love.graphics.pop()
end

return tentacle
