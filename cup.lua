local cup = {}
cup.__index = cup

function cup.new(world, x, y)
  local t = {}
  setmetatable(t, cup)

  t.world = world

  t.complete = false
  t.fill = 1

  t.body = love.physics.newBody(world, x, y, 'dynamic')
  t.shape = love.physics.newPolygonShape(
              -65, -90,
              65, -90,
              50, 100,
              -50, 100
            )
  t.fixture = love.physics.newFixture(t.body, t.shape)
  t.fixture:setRestitution(0.5)

  t.image = love.graphics.newImage('art/cup.0001.png')
  t.particle_image = love.graphics.newImage('art/bubble.png')

  t.particles = love.graphics.newParticleSystem(t.particle_image, 5000)
  t.particles:setColors(0, 255, 255, 128,
                        0, 255, 255, 0)
  t.particles:setEmissionRate(100)
  t.particles:setParticleLifetime(0.5)
  t.particles:setDirection(math.pi/2)
  t.particles:setSpeed(900)
  t.particles:setSizes(0.2)

  t.sound = love.audio.newSource('music/water.ogg', 'static')
  t.sound:setLooping(true)
  t.sound:play()
  t.sound:pause()

  return t
end

function cup:destroy()
  self.body:destroy()
end

function cup:draw()
  if debug then
    love.graphics.print(self.body:getAngle() % math.pi*2, 10, 10)
    if volume then
      love.graphics.print(volume, 10, 20)
    end
    love.graphics.print(self.fill, 10, 30)
    love.graphics.print(self.sound:isPaused() and 'true' or 'false', 10, 40)
  end

  love.graphics.draw(self.particles)

  love.graphics.setColor(200, 200, 200)
  love.graphics.push()
  love.graphics.translate(self.body:getPosition())
  love.graphics.rotate(self.body:getAngle())
  love.graphics.draw(self.image, -180, -180, 0, 0.25, 0.25)
  love.graphics.pop()
end

function cup:update(dt)
  local x, y = self.body:getLocalPoint(self.body:getPosition())
  local angle = self.body:getAngle() % (math.pi*2)
  volume = 1 - math.abs(angle - math.pi) / math.pi
  volume = math.max(0, volume - math.sqrt(1 - self.fill) / 2)
  if volume > self.fill then
    volume = self.fill
  end
  local w = math.sqrt(math.abs(math.cos((angle - math.pi)/2))) * 20
  local offset = -65 + w
  if angle < math.pi then
    offset = 65 - w
  end
  x, y = self.body:getWorldPoint(x + offset, y - 90 + volume*20)
  self.particles:setPosition(x, y)
  self.particles:setAreaSpread('uniform', math.max(w, 1), 1)
  self.particles:setEmissionRate(volume * 1000)
  self.particles:update(dt)

  self.fill = math.max(0, self.fill - volume*dt*2)
  if self.fill < 0.05 then
    self.fill = 0
    self.complete = true
  end

  if volume <= 0.01 then
    self.sound:pause()
  else
    self.sound:resume()
  end
end

return cup
