local scissors = {}
scissors.__index = scissors

function scissors.new(world, x, y)
  local t = {}
  setmetatable(t, scissors)
  t.world = world

  t.progress = 0
  t.complete = false
  
  t.lblade_body = love.physics.newBody(world, x, y, 'dynamic')
  t.lblade_shape1 = love.physics.newRectangleShape(0, -70, 40, 340)
  t.lblade_shape2 = love.physics.newCircleShape(-36, 205, 30)
  t.lblade_shape3 = love.physics.newCircleShape(-36, 135, 30)
  t.lblade_shape4 = love.physics.newRectangleShape(-36, 170, 60, 70)
  love.physics.newFixture(t.lblade_body, t.lblade_shape1)
  love.physics.newFixture(t.lblade_body, t.lblade_shape2)
  love.physics.newFixture(t.lblade_body, t.lblade_shape3)
  love.physics.newFixture(t.lblade_body, t.lblade_shape4)

  t.rblade_body = love.physics.newBody(world, x, y, 'dynamic')
  t.rblade_shape1 = love.physics.newRectangleShape(0, -70, 40, 340)
  t.rblade_shape2 = love.physics.newCircleShape(36, 205, 30)
  t.rblade_shape3 = love.physics.newCircleShape(36, 135, 30)
  t.rblade_shape4 = love.physics.newRectangleShape(36, 170, 60, 70)
  love.physics.newFixture(t.rblade_body, t.rblade_shape1)
  love.physics.newFixture(t.rblade_body, t.rblade_shape2)
  love.physics.newFixture(t.rblade_body, t.rblade_shape3)
  love.physics.newFixture(t.rblade_body, t.rblade_shape4)

  t.joint = love.physics.newRevoluteJoint(t.rblade_body, t.lblade_body, x, y, false)
  t.joint:setLimitsEnabled(true)
  t.joint:setLimits(0.08, 2)

  t.image = love.graphics.newImage('art/scissors.0001.png')
  t.sfx_open = love.audio.newSource('music/scissors-open.ogg', 'static')
  t.sfx_close = love.audio.newSource('music/scissors-close.ogg', 'static')

  return t
end

function scissors:destroy()
  self.lblade_body:destroy()
  self.rblade_body:destroy()
end

function scissors:draw()
  love.graphics.setColor(255, 255, 255)
  if debug then
    love.graphics.print(self.progress, 10, 10)
  end

  love.graphics.push()
  love.graphics.translate(self.lblade_body:getPosition())
  love.graphics.rotate(self.lblade_body:getAngle())
  love.graphics.draw(self.image, -288, 266, -math.pi/2, 0.4, 0.4)
  love.graphics.pop()

  love.graphics.push()
  love.graphics.translate(self.rblade_body:getPosition())
  love.graphics.rotate(self.rblade_body:getAngle())
  love.graphics.draw(self.image, 288, 266, -math.pi/2, 0.4, -0.4)
  love.graphics.pop()
end

function scissors:update(dt)
  local lower, upper = self.joint:getLimits()
  local angle = self.lblade_body:getAngle() - self.rblade_body:getAngle()
  if self.progress == 0 and angle > lower + 0.1 then
    self.sfx_open:play()
    self.progress = 1
  elseif self.progress == 1 and angle > upper - 0.1 then
    self.progress = 2
  elseif self.progress ~= 0 and angle < lower + 0.1 then
    if self.progress == 2 then
      self.complete = true
    end
    self.sfx_close:play()
    self.progress = 0
  end
end

return scissors
