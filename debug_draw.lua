return function(world)
  for _, body in ipairs(world:getBodyList()) do
    local x, y = body:getPosition()
    love.graphics.line(x-3, y, x+3, y)
    love.graphics.line(x, y-3, x, y+3)
    for _, fixture in ipairs(body:getFixtureList()) do
      local shape = fixture:getShape()
      if shape:getType() == 'polygon' then
        love.graphics.polygon('line', body:getWorldPoints(shape:getPoints()))
      elseif shape:getType() == 'circle' then
        local x, y = body:getWorldPoint(shape:getPoint())
        love.graphics.circle('line', x, y, shape:getRadius())
      end
    end
  end
end
