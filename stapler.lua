local stapler = {}
stapler.__index = stapler

function stapler.new(world, x, y)
  local t = {}
  setmetatable(t, stapler)

  t.world = world

  t.complete = false
  t.lever_position = 0

  y = y + 200

  t.base_body = love.physics.newBody(world, x, y, 'dynamic')
  t.base_shape1 = love.physics.newRectangleShape(-80, -150, 20, 400)
  t.base_shape2 = love.physics.newRectangleShape(-25, -20, 90, 140)
  love.physics.newFixture(t.base_body, t.base_shape1)
  love.physics.newFixture(t.base_body, t.base_shape2)

  t.lever_body = love.physics.newBody(world, x, y, 'dynamic')
  --t.lever_shape = love.physics.newRectangleShape(0, -140, 90, 380)
  t.lever_shape = love.physics.newPolygonShape(
                    -45, -325,
                    0, -335,
                    35, -310,
                    50, -240,
                    40, 0,
                    30, 30,
                    10, 45,
                    -45, 45)
  love.physics.newFixture(t.lever_body, t.lever_shape)

  t.joint = love.physics.newRevoluteJoint(t.base_body, t.lever_body, x, y, false)
  t.joint:setLimitsEnabled(true)
  t.joint:setLimits(-0.08, -0.01)
  t.joint:setMotorEnabled(true)
  t.joint:setMotorSpeed(100000)
  t.joint:setMaxMotorTorque(50000000)

  t.staples = {}

  t.top_image = love.graphics.newImage('art/swegline-top.0001.png')
  t.bottom_image = love.graphics.newImage('art/swegline-bottom.0001.png')
  t.staple_image = love.graphics.newImage('art/staple.0001.png')

  t.sound = love.audio.newSource('music/stapler.ogg', 'static')

  return t
end

function stapler:destroy()
  self.base_body:destroy()
  self.lever_body:destroy()
  for _, staple in ipairs(self.staples) do
    staple.b:destroy()
  end
end

function stapler:draw()
  love.graphics.setColor(200, 200, 200)
  if debug and angle then
    love.graphics.print(self.lever_position, 10, 10)
    love.graphics.print(angle, 10, 20)
    love.graphics.print(#self.staples, 10, 30)
  end

  for _, staple in ipairs(self.staples) do
    love.graphics.push()
    love.graphics.translate(staple.b:getPosition())
    love.graphics.rotate(staple.b:getAngle())
    love.graphics.draw(self.staple_image, 25, 5, math.pi, 0.8, 0.8)
    love.graphics.pop()
  end

  love.graphics.push()
  love.graphics.translate(self.lever_body:getPosition())
  love.graphics.rotate(self.lever_body:getAngle())
  love.graphics.draw(self.bottom_image, -330, -520, 0, 0.5, 0.5)
  love.graphics.pop()

  love.graphics.push()
  love.graphics.translate(self.base_body:getPosition())
  love.graphics.rotate(self.base_body:getAngle())
  love.graphics.draw(self.top_image, -330, -520, 0, 0.5, 0.5)
  love.graphics.pop()

  if debug then
    love.graphics.push()
    love.graphics.setColor(255, 0, 255)
    local x, y = self.base_body:getLocalPoint(self.base_body:getPosition())
    x = x - 40
    y = y - 350
    love.graphics.translate(self.base_body:getWorldPoint(x, y))
    love.graphics.line(-5, 0, 5, 0)
    love.graphics.line(0, -5, 0, 5)
    love.graphics.pop()
  end
end

function stapler:update(dt)
  local lower, upper = self.joint:getLimits()
  angle = self.lever_body:getAngle() - self.base_body:getAngle()
  if self.lever_position == 1 and angle >= upper then
    self:addStaple()
    self.complete = true
    self.lever_position = 0
  elseif self.lever_position == 0 and angle <= lower - 0.02 then
    self.sound:play()
    self.lever_position = 1
  end
end

function stapler:addStaple()
  local x, y = self.base_body:getLocalPoint(self.base_body:getPosition())
  x, y = self.base_body:getWorldPoint(x - 40, y - 320)

  local s = {}
  s.b = love.physics.newBody(self.world, x, y, 'dynamic')
  s.s = love.physics.newRectangleShape(50, 5)
  s.f = love.physics.newFixture(s.b, s.s)

  s.b:setAngularVelocity(math.random(-10, 10)/10)

  table.insert(self.staples, s)
end

return stapler
